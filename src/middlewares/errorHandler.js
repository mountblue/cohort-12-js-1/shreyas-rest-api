const winstonLogger = require('../config/logger');

const initialErrorHandler = (req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
};

const finalErrorHandler = (error, req, res, next) => {
  error.status = error.status || 500;
  winstonLogger.error(`${error.status}: ${error.message}`);
  res.status(error.status).send(error.message);
};

module.exports = {
  initialErrorHandler,
  finalErrorHandler
};
