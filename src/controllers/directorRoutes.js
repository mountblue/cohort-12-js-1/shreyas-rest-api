const express = require('express');

const {
  directorIdValidation,
  directorNameValidation
} = require('../validations/directorValidations');

const {
  getSpecificDirector,
  getAllDirectors,
  insertSpecificDirector,
  updateSpecificDirector,
  deleteSpecificDirector,
  directorExists
} = require('../models/directorModels');

const router = express.Router();

router.use(express.json());

// Get request for displaying all directors
router.get('/', async (req, res) => {
  try {
    const allDirectors = await getAllDirectors();
    res.json(allDirectors.rows);
  } catch (error) {
    console.error(error);
  }
});

// Get request for displaying specific director
router.get('/:directorId', async (req, res) => {
  try {
    const result = directorIdValidation({ directorId: req.params.directorId });
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      const specificDirector = await getSpecificDirector(req.params.directorId);
      if (specificDirector.rows.length === 0) {
        res
          .status(404)
          .send(`Director with id ${req.params.directorId} not found`);
      }
      res.json(specificDirector.rows[0]);
    }
  } catch (error) {
    console.error(error);
  }
});

// Post request for adding a director
router.post('/', async (req, res) => {
  try {
    const result = directorNameValidation(req.body);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      await insertSpecificDirector(req.body.directorName);
      const allDirectors = await getAllDirectors();
      res.json(allDirectors.rows);
    }
  } catch (error) {
    console.error(error);
  }
});

// Put request for modifying a director's details if he exists
router.put('/:directorId', async (req, res) => {
  try {
    const specificDirector = await getSpecificDirector(req.params.directorId);
    if (specificDirector.rows.length === 0) {
      res
        .status(404)
        .send(`Director with id ${req.params.directorId} not found`);
    }
    const result = directorNameValidation(req.body);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      await updateSpecificDirector(
        req.body.directorName,
        req.params.directorId
      );
      const allDirectors = await getAllDirectors();
      res.json(allDirectors.rows);
    }
  } catch (error) {
    console.error(error);
  }
});

// Delete request to delete a director's details if he exists
router.delete('/:directorId', async (req, res) => {
  try {
    const result = directorIdValidation({ directorId: req.params.directorId });
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else if (await directorExists(req.params.directorId)) {
      res
        .status(400)
        .send(`Director with id ${req.params.directorId} does not exist`);
    } else {
      await deleteSpecificDirector(req.params.directorId);
      const allDirectors = await getAllDirectors();
      res.json(allDirectors.rows);
    }
  } catch (error) {
    console.error(error);
  }
});

module.exports = router;
