const express = require('express');

const {
  movieIdValidation,
  movieNameAndIdValidation
} = require('../validations/moviesValidations');

const {
  getSpecificMovie,
  getAllMovies,
  insertSpecificMovie,
  updateSpecificMovie,
  deleteSpecificMovie,
  movieExists
} = require('../models/moviesModels');

const { directorExists } = require('../models/directorModels');

const router = express.Router();

router.use(express.json());

router.get('/', async (req, res) => {
  try {
    const allMovies = await getAllMovies();
    res.json(allMovies.rows);
  } catch (error) {
    console.error(error);
  }
});

router.get('/:movieId', async (req, res) => {
  try {
    const result = movieIdValidation({ movieId: req.params.movieId });
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else {
      const specificMovie = await getSpecificMovie(req.params.movieId);
      if (specificMovie.rows.length === 0) {
        res.status(404).send(`Movie with id ${req.params.movieId} not found`);
      }
      res.json(specificMovie.rows[0]);
    }
  } catch (error) {
    console.error(error);
  }
});

router.post('/', async (req, res) => {
  try {
    const result = movieNameAndIdValidation(req.body);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else if (await directorExists(req.body.directorId)) {
      res
        .status(400)
        .send(
          'Director does not exist. Please create director before adding his movie'
        );
    } else {
      await insertSpecificMovie(req.body.directorId, req.body.movieName);
      const allMovies = await getAllMovies();
      res.json(allMovies.rows);
    }
  } catch (error) {
    console.error(error);
  }
});

router.put('/:movieId', async (req, res) => {
  try {
    const specificMovie = await getSpecificMovie(req.params.movieId);
    if (specificMovie.rows.length === 0) {
      res.status(404).send(`Movie with id ${req.params.movieId} not found`);
    }
    const result = movieNameAndIdValidation(req.body);
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else if (await directorExists(req.body.directorId)) {
      res
        .status(400)
        .send(
          'Director does not exist. Please create director before updating their movie'
        );
    } else {
      await updateSpecificMovie(req.body.movieName, req.body.directorId);
      const allMovies = await getAllMovies();
      res.json(allMovies.rows);
    }
  } catch (error) {
    console.error(error);
  }
});

router.delete('/:movieId', async (req, res) => {
  try {
    const result = movieIdValidation({ movieId: req.params.movieId });
    if (result.error) {
      res.status(400).send(result.error.details[0].message);
    } else if (await movieExists(req.params.movieId)) {
      res.status(400).send('Movie does not exist');
    } else {
      await deleteSpecificMovie(req.params.movieId);
      const allMovies = await getAllMovies();
      res.json(allMovies.rows);
    }
  } catch (error) {
    console.error(error);
  }
});

module.exports = router;
